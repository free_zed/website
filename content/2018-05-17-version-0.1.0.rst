Version 0.1.0
=============

:slug: 2018-05-17-version-0.1.0
:lang: fr
:date: 2018-05-17
:author: Guillaume Ayoub

Grande nouvelle !

``pip install watchghost``

Après des mois et des mois de dur labeur, la première version de WatchGhost est
enfin disponible sur PyPI. Nous avons également mis en ligne de `la
documentation <http://watchghost.readthedocs.io>`_ pour que vous puissiez
installer et configurer le gentil fantôme chez vous.

N'hésitez pas à participer sur `GitLab
<https://gitlab.com/localg-host/watchghost>`_ !
