Watchghost version 0.2.1
========================

:slug: 2018-11-07-version-0.2.1
:lang: fr
:date: 2018-11-07
:author: Sam

La version 0.2.1 est disponible sur `PyPI <https://pypi.org/project/watchghost/>`_.

Parmi les nouveautés, la console web s'offre un relooking (aperçu lors de la `PyConFR 2018 <https://www.youtube.com/watch?v=arJhcm1_fiQ>`_). La console web a été réécrite avec le framework javascript `Vue.js <https://vuejs.org/>`_ et la bibliothèque CSS `Bootstrap 4 <http://getbootstrap.com/>`_.

Afin de réaliser cette nouvelle console web, nous avons réalisé plusieurs URLs permettant de lister les différentes ressources définies dans WatchGhost.

La documentation a été corrigée : les exemples de configuration étaient du Python alors qu'ils auraient dû être au format JSON.

Une correction a été apportée aux éléments nécessitant une configuration TLS afin d'être compatible avec les changements réalisés en Python 3.7.

Enfin, l'empaquetage Python a été mis à jour afin de tirer profit de la configuration via le fichier setup.cfg. En outre toutes les configurations des outils Python ont été centralisées dans ce fichier. Une erreur dans cette configuration nous a amené à sortir une version 0.2.1 très rapidement après la sortie de la version 0.2.0 : le champ ``license`` est destiné à recevoir le nom de la licence mais pas son contenu complet. Il semble qu'un champ ``license_file`` soit en préparation pour les prochaines versions de `setuptools <https://pypi.org/project/setuptools/>`_ (cf `la pull request 1536 de setuptools <https://github.com/pypa/setuptools/pull/1536>`_).

La commande pour lancer WatchGhost a également évolué :

.. code-block:: shell

  pip install watchghost
  watchghost

En développement, on peut lancer l'application grâce à la commande ``python -m watchghost``.

➡️ plus d'information dans la `documentation <http://watchghost.readthedocs.io>`_.
