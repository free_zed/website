À propos de Localghost
======================

:slug: 2016-09-17-a-propos
:lang: fr
:date: 2016-09-17
:author: Arthur Vuillard

Localghost est un regroupement de développeu·rs·ses s'occupant également de l'administration système des applications qu'ils·elles développent.

Localghost est né de l'envie de **mieux** faire dans l'exploitation d'applications informatiques. Cette envie se traduit par **le développement d'outils**, **la mise en place de techniques** et **le partage de ces éléments** au sein du groupe mais aussi au plus grand nombre.

Notre proposition
-----------------

Notre premier champ d'action est **la surveillance du fonctionnement de nos applications**. Il s'agit là de mettre en place des sondes permettant de détecter automatiquement les dysfonctionnements et leurs résolutions, et d'alerter un humain en cas de problème.

Nous nous intéressons également à la **reproductibilité des déploiements**. C'est-à-dire faire en sorte que le déploiement d'une application soit facilement réalisable d'un serveur à un autre. Il s'agit de formaliser la description de ces déploiements pour pouvoir les relancer aisément.

Enfin, nous souhaitons assurer la mise en place de **Plans de Reprise d'Activité** (PRA). Un PRA définit les étapes pour remonter un service si le déploiement initial n'est plus disponible. Pour ce faire, la reproductibilité des déploiements dont nous avons parlé au paragraphe précédent est nécessaire tout comme l'instauration de bonnes pratiques au niveau des sauvegardes et de leur restauration.

Nos valeurs
-----------

Nous sommes issus de la communauté du **Logiciel Libre**. Il est important que nos productions soient diffusées le plus largement possible et puissent être utiles au plus grand nombre de personnes.

Le partage est un élément fondateur de notre regroupement. Nous sommes issus de 3 entreprises différentes et nous nous sommes regroupés de manière informelle pour discuter et échanger sur nos pratiques respectives.

Nos membres
-----------

Nos membres fondateurs sont principalement issus de la communauté Python de Lyon, mais nous ne nous limitons ni au langage de programmation ni à la position géographique de chaque personne.

Vous pouvez retrouver nos membres sur `notre groupe Gitlab <https://gitlab.com/groups/localg-host/group_members>`_.

Notre nom
---------

Il est rigolo (notre hostname aussi).

Notre logo
----------

Il est trop mignon.

Participer ?
------------

Si vous souhaitez participer, `rejoignez nous sur Gitlab <https://gitlab.com/localg-host/>`_ ou écrivez nous à l'adresse `contact@localg.host <contact@localg.host>`_.
