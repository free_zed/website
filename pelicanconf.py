#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'collectif localg.host'
SITENAME = 'localg.host'
SITEURL = ''

PATH = 'content'
THEME = 'theme'


TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

LOCALE = 'fr_FR.utf8'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

# Social widget
SOCIAL = (
    ('envelope', 'mailto:contact@localg.host'),
    ('rss', '/feeds/all.atom.xml'),
    ('mastodon', 'https://mastodon.social/@localg_host'),
    ('twitter', 'https://twitter.com/localg_host'),
    ('gitlab', 'https://gitlab.com/localg-host/'),
)

DEFAULT_PAGINATION = 10

INDEX_SAVE_AS = 'nouvelles.html'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

HEADER_COLOR = "#065daa"
